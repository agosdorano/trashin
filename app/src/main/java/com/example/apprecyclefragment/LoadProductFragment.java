package com.example.apprecyclefragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class LoadProductFragment extends DialogFragment {

    public String getRawValue() {
        return rawValue;
    }

    public void setRawValue(String rawValue) {
        this.rawValue = rawValue;
        Log.d("msg","received raw val: "+this.rawValue);
    }

    private String rawValue;

    public LoadProductFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        final View view = inflater.inflate(R.layout.load_product_fragment,container);

        Button btnClose = (Button)view.findViewById(R.id.button);
        Button btnUpload = (Button)view.findViewById(R.id.button2);

        btnClose.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                dismiss();

            }
        });
        btnUpload.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                EditText text = (EditText)view.findViewById(R.id.editText);
                String information = text.getText().toString();
                Log.d("msg",information);
                if(checkInput(information)) {
                    loadProductInfo(rawValue, information);
                    dismiss();
                }
                else Toast.makeText(getActivity().getApplicationContext(),R.string.no_valid,Toast.LENGTH_SHORT).show();

            }
        });

        return view;
    }

    public void loadProductInfo(String rawValue, String info) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> product = new HashMap<>();
        product.put("rawValue", rawValue);
        product.put("info", info);
        // aggiungo ora l'elemento alla collezione di documenti, dove ogni documento è un "obj" json
        final Context context = getActivity().getApplicationContext();
        db.collection("products").add(product).
                addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("msg", "DocumentSnapshot added with ID: " + documentReference.getId());
                        Toast.makeText(context,R.string.success,Toast.LENGTH_SHORT).show();

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("msg", "Error adding document", e);
                        Toast.makeText(context,R.string.problem,Toast.LENGTH_SHORT).show();
                    }
                });

    }


    private boolean checkInput(String informations){
        boolean valid = false;
        if(!informations.isEmpty()){ // possibile anche aggiungere altre condizioni che pero ora non so
            valid = true;
        }
        return valid;
    }


}
