package com.example.apprecyclefragment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecycleViewerActivity extends AppCompatActivity {

    public static final float strokeWidht = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycle_viewer);
        Log.d("msg", "sono nella seconda activity");
        Intent intent = getIntent();
        String image_path = intent.getStringExtra("img");
        Uri fileUri = Uri.parse(image_path);
        Log.d("msg", fileUri.toString());
        // setto l'immagine comunque nel caso non venga riconosciuta dalla funzione apposita
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setImageURI(fileUri);
        recognizeBarcodes(fileUri);
    }

    private void recognizeBarcodes(final Uri fileUri) {
        try {
            Log.d("msg", " pre firebasevisionimage creation");
            FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(fromUriToBitamp(fileUri));
            Log.d("msg", " post firebasevisionimage creation");
            FirebaseVisionBarcodeDetector detector = FirebaseVision.getInstance().getVisionBarcodeDetector();
            final TextView tv = findViewById(R.id.textView);
            Task<List<FirebaseVisionBarcode>> result = detector.detectInImage(image)
                    .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionBarcode>>() {
                        @Override
                        public void onSuccess(List<FirebaseVisionBarcode> barcodes) {
                            // Task completed successfully
                            // ...
                            Bitmap image = fromUriToBitamp(fileUri);
                            Log.d("msg", " barcode detected");
                            if (barcodes.isEmpty()) tv.setText(R.string.no_recognized);
                            else {
                                /*
                                int i = 1;
                                for (FirebaseVisionBarcode barcode : barcodes) { // forse meglio fare che ne riconosce uno solo al momento
                                    Rect bounds = barcode.getBoundingBox();
                                    image = drawRectangle(image, bounds);
                                    String rawValue = barcode.getRawValue();
                                    searchProductInfo(rawValue, i);
                                    i++;
                                }*/
                                FirebaseVisionBarcode barcode = barcodes.get(0);
                                Rect bounds = barcode.getBoundingBox();
                                image = drawRectangle(image, bounds);
                                String rawValue = barcode.getRawValue();
                                searchProductInfo(rawValue, 1);
                                ImageView imageView = (ImageView) findViewById(R.id.imageView);
                                imageView.setImageDrawable(new BitmapDrawable(getResources(), image));
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            // Task failed with an exception
                            // ...
                            Log.d("msg", "issues detected");

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private Bitmap drawRectangle(Bitmap b, Rect bounds) {
        // proviamo a costruire un rettangolo sull'immagine per evidenziare i bar code
        // Rectangle Objects
        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setStyle(Paint.Style.STROKE); // disegna solo i bordi
        p.setStrokeWidth(strokeWidht); // imposta la dimensione del bordo
        p.setColor(Color.GREEN); // setto il colore
        // Create Temp bitmap
        Bitmap tBitmap = Bitmap.createBitmap(b.getWidth(), b.getHeight(), Bitmap.Config.RGB_565);
        // Create a new canvas and add Bitmap into it
        Canvas tCanvas = new Canvas(tBitmap);
        //Draw the image bitmap into the canvas
        tCanvas.drawBitmap(b, 0, 0, null);
        // Draw a rectangle over canvas
        tCanvas.drawRoundRect(new RectF(bounds), 2, 2, p);
        return tBitmap;
    }

    private Bitmap fromUriToBitamp(Uri fileUri) {
        /* affrontiamo il seguente problema: se img è passata dalla fotocamera quindi memorizzata
        nella nostra sandbox allora la dobbiamo leggere con decodeFile; viceversa se proviena dalla
        galleria con getBitmap soluzione: analizzare il contenuto della stringa ricevuta:
        se c'è un token dell'elemento che riceviamo in input uguale a contentprovider ci adattiamo
         */
        Bitmap b = null;
        try {
            String path = fileUri.toString();
            if (path.contains("contentprovider"))
                b = MediaStore.Images.Media.getBitmap(this.getContentResolver(), fileUri);
            else b = BitmapFactory.decodeFile(fileUri.toString());
        } catch (IOException e) {
            //e.printStackTrace();

        }
        return b;
    }

    public void searchProductInfo(final String rawValue, final int i) {
        readData(rawValue, new FirestoreCallbackRead() {
            @Override
            public void onCallback(String information) {
                if (information == null) {
                    // se è nullo costruisco un dialog che mi fa inserire informazioni sull'oggetto
                    // e chiede di caricarlo
                    FragmentManager fm = getSupportFragmentManager();
                    LoadProductFragment f = new LoadProductFragment();
                    f.setRawValue(rawValue);
                    f.show(fm,"fragment_edit_name");
                } else {
                    TextView tv = findViewById(R.id.textView);
                    tv.append(String.valueOf(i) + ". " + information + "\n");

                }
            }
        });
    }
    private void readData(String rawValue, final FirestoreCallbackRead firestoreCallbackRead) {
        final String[] information = {null};
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("products").whereEqualTo("rawValue", rawValue)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (!task.getResult().isEmpty()) {
                                information[0] = ((String) task.getResult().getDocuments().get(0).getData().get("info"));
                                //Log.d("msg", "getting documents: " +getInformation());
                            } else {
                                Log.d("msg", "no information about rawval");
                            }
                            firestoreCallbackRead.onCallback(information[0]);
                        } else {
                            Log.d("msg", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }


    /*** DEFINZIONI INTERFACCE DI CALLBACK ***/

    private interface FirestoreCallbackRead {
        void onCallback(String information);
    }
}

/*

funzioni non piu usate



    public void loadProductInfo(String rawValue, String info) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> product = new HashMap<>();
        product.put("rawValue", rawValue);
        product.put("info", info);

        // aggiungo ora l'elemento alla collezione di documenti, dove ogni documento è un "obj" json
        db.collection("products").add(product).
                addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("msg", "DocumentSnapshot added with ID: " + documentReference.getId());

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("msg", "Error adding document", e);
                    }
                });

    }


 */